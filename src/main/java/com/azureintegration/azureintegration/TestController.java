package com.azureintegration.azureintegration;

import java.util.Map;
import java.util.Optional;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.annotation.RegisteredOAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.oidc.StandardClaimAccessor;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.Gson;
import com.microsoft.graph.models.Group;
import com.microsoft.graph.models.User;

@RestController
public class TestController {

	@GetMapping("Admin")
	@ResponseBody
	@PreAuthorize("hasAuthority('APPROLE_Admin')")
	public String Admin() {
		return "Admin message";
	}

	@GetMapping("/graph")
	@ResponseBody
	public String graph(
			@RegisteredOAuth2AuthorizedClient("graph") OAuth2AuthorizedClient graphClient,
			OAuth2AuthenticationToken authentication,
			@RegisteredOAuth2AuthorizedClient("azure") OAuth2AuthorizedClient azureClient
	) throws JsonProcessingException {

		System.out.println(authentication.getName());

		String preferredUsername = Optional.of(authentication)
				.map(OAuth2AuthenticationToken::getPrincipal)
				.map(user -> (OidcUser) user)
				.map(StandardClaimAccessor::getPreferredUsername)
				.orElse("UNKNOWN");

		System.out.println(preferredUsername);

		System.out.println(azureClient.getClientRegistration().getClientName());

		Map<String, UsuarioAD> retorno = Utilities.graphUserProperties(graphClient);

		Gson gson = new Gson();

		return gson.toJson(retorno);
	}

}


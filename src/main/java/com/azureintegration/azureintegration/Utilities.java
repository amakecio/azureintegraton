package com.azureintegration.azureintegration;

// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import com.microsoft.graph.authentication.BaseAuthenticationProvider;
import com.microsoft.graph.models.Group;
import com.microsoft.graph.models.User;
import com.microsoft.graph.requests.GraphServiceClient;
import com.microsoft.graph.requests.GroupCollectionPage;
import com.microsoft.graph.requests.GroupCollectionRequest;

import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;

public class Utilities {
	private Utilities() {
		throw new IllegalStateException("Utility class. Don't instantiate");
	}

	/**
	 * Take a subset of ID Token claims and put them into KV pairs for UI to display.
	 * @param principal OidcUser (see SampleController for details)
	 * @return Map of filteredClaims
	 */
	public static Map<String,String> filterClaims(OidcUser principal) {
		final String[] claimKeys = {"sub", "aud", "ver", "iss", "name", "oid", "preferred_username"};
		final List<String> includeClaims = Arrays.asList(claimKeys);

		Map<String,String> filteredClaims = new HashMap<>();
		includeClaims.forEach(claim -> {
			if (principal.getIdToken().getClaims().containsKey(claim)) {
				filteredClaims.put(claim, principal.getIdToken().getClaims().get(claim).toString());
			}
		});
		return filteredClaims;
	}

	/**
	 * Take a few of the User properties obtained from the graph /me endpoint and put them into KV pairs for UI to display.
	 * @param graphAuthorizedClient OAuth2AuthorizedClient created by AAD Boot starter. See the SampleController class for details.
	 * @return Map<String,String> select Key-Values from User object
	 */
	public static Map<String,UsuarioAD> graphUserProperties(OAuth2AuthorizedClient graphAuthorizedClient) {
		final GraphServiceClient graphServiceClient = Utilities.getGraphServiceClient(graphAuthorizedClient);
		final User user = graphServiceClient.me().buildRequest().get();
		Map<String,UsuarioAD> userProperties = new HashMap<>();

		GroupCollectionRequest groupsRequest = graphServiceClient.groups().buildRequest().top(999);
		GroupCollectionPage groupsCollection = groupsRequest.get();
		List<Group> grupos = groupsCollection.getCurrentPage();

		UsuarioAD usuarioAD = new UsuarioAD();

		if (user == null) {
			userProperties.put("Graph Error", null);
		} else {

			usuarioAD.setUser(user);
			usuarioAD.setGrupos(grupos);

			userProperties.put("user", usuarioAD);
		}

		return userProperties;
	}

	/**
	 * getGraphServiceClient prepares and returns a graphServiceClient to make API calls to
	 * Microsoft Graph. See docs for GraphServiceClient (GraphSDK for Java v3).
	 *
	 * Since the app handles token acquisition through AAD boot starter, we can give GraphServiceClient
	 * the ability to use this access token when it requires it. In order to do this, we must create a
	 * custom AuthenticationProvider (GraphAuthenticationProvider, see below).
	 *
	 *
	 * @param graphAuthorizedClient OAuth2AuthorizedClient created by AAD Boot starter. Used to surface the access token.
	 * @return GraphServiceClient GraphServiceClient
	 */

	public static GraphServiceClient getGraphServiceClient(OAuth2AuthorizedClient graphAuthorizedClient) {
		return GraphServiceClient.builder().authenticationProvider(new GraphAuthenticationProvider(graphAuthorizedClient))
				.buildClient();
	}

	/**
	 * Sample GraphAuthenticationProvider class. An Authentication provider is required for setting up a
	 * GraphServiceClient. This one extends BaseAuthenticationProvider which in turn implements IAuthenticationProvider.
	 * This allows using an Access Token provided by Oauth2AuthorizationClient.
	 */
	private static class GraphAuthenticationProvider
			extends BaseAuthenticationProvider {

		private OAuth2AuthorizedClient graphAuthorizedClient;

		/**
		 * Set up the GraphAuthenticationProvider. Allows accessToken to be
		 * used by GraphServiceClient through the interface IAuthenticationProvider
		 *
		 * @param graphAuthorizedClient OAuth2AuthorizedClient created by AAD Boot starter. Used to surface the access token.
		 */
		public GraphAuthenticationProvider( OAuth2AuthorizedClient graphAuthorizedClient) {
			this.graphAuthorizedClient = graphAuthorizedClient;
		}

		/**
		 * This implementation of the IAuthenticationProvider helps injects the Graph access
		 * token into the headers of the request that GraphServiceClient makes.
		 *
		 * @param requestUrl the outgoing request URL
		 * @return a future with the token
		 */
		@Override
		public CompletableFuture<String> getAuthorizationTokenAsync( final URL requestUrl){
			return CompletableFuture.completedFuture(graphAuthorizedClient.getAccessToken().getTokenValue());
		}
	}
}

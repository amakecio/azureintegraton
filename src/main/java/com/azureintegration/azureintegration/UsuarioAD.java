package com.azureintegration.azureintegration;

import java.util.List;

import com.microsoft.graph.models.Group;
import com.microsoft.graph.models.User;

public class UsuarioAD{

	User user;
	List<Group> grupos;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Group> getGrupos() {
		return grupos;
	}

	public void setGrupos(List<Group> grupos) {
		this.grupos = grupos;
	}
}
